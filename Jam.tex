

\documentclass[10pt, conference]{IEEEtran}

\ifCLASSINFOpdf
 
\else
\fi

\hyphenation{Bug Fix Verification An Analysis of Pull Request Acceptance on the Tree-Structured Level}

\usepackage{graphicx}
\graphicspath{ {images/} }
\usepackage{booktabs,fixltx2e}
\usepackage[font={small,it}]{caption}
\usepackage[flushleft]{threeparttable}
\usepackage{amstext} 
\usepackage{cite}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Architectural Invariant Inference In ROS Based Systems (Robot Operating System)}


% author names and affiliations
% use a multiple column layout for up to two different
% affiliations

\author{\IEEEauthorblockN{Jam Marcos Hernandez Quiceno}
\IEEEauthorblockA{Institute of Software Research - Carnegie Mellon University\\State University of New York at Potsdam\\hernanjm199@potsdam.edu}
}
\maketitle


\begin{abstract}
Understanding the architectural properties that hold on a ROS (Robot Operating System) system can be difficult as result of the constraints that the framework itself can carry and the extent of complexity in which the program can evolve to. Understanding such complexity in terms of invariants provides to developers a new approach for debugging and validating ROS systems. In this paper we present our approach on recording invariants in subscribers, publishers and service calls. We do this by launching three nodes that constantly monitor the system's architecture, two of which monitor the nodes subscribing and/or publishing to certain node/s, the last one is in charge of recording service calls. These nodes report the architectural changes made throughout execution. We boil down the provided data by presenting only properties that hold in the report. We demonstrate the working tool on a functioning Turtlebot. This is exciting because understanding such conditions in a system can help in the process of debugging and validation. In a future self-adaptive system; a tool that provides accurate information regarding the system's invariants would be able to help validate a patch without human intervention. 
\end{abstract}
\IEEEpeerreviewmaketitle

\section{Introduction}
\label{Introduction}
As the complexity of a system increases, the understanding of that system decreases. This can be observed in ROS (Robot Operating System) systems, a framework that allows developers to put together robotic applications. There are many conditions that ROS systems rely on in order to function correctly. These conditions can grow as the system expands and new developers contribute. But not all conditions are explicitly stated by the developers. These unstated conditions may be critical and could be easily violated by unaware contributors to the project. We can assume that this is a constant throughout many projects, not every condition will be explicitly stated. By understanding the lack in documentation and contemplating the usefulness of recognizing such conditions, we built a tool that monitors the state of a system and reports the data to later be analyzed.

Invariants are properties that hold throughout a program point or program points. There are many uses for a tool that is capable of reporting candidate invariants in a ROS system. Such report can help developers debug, validate programs and for a future self-adaptive system, the understating of its invariants could help validate patches without human intervention. Our approach intends to excel the understanding of a system's architecture, by introducing a technique capable of reporting the necessary data to infer invariants. Architecture is critical, relevant and it is a new ground to study in relation to inferring invariants in ROS systems. Identifying the connections present throughout a program's execution is the first logical step in discovering relevant conditions that hold on a system.

Understanding connections between nodes can provide a clear structure of a ROS program. We  generate set of possible invariants that may not only be present in a one to one relation but in a chain of connections. These chain of connections may depend on each other and may even be critical for the correct functionality of the system. However developers do not provide explicit information about their existence. In many cases developers do not recognize such connections, which leaves gaps of knowledge (present invariants) for the future development of the system. These gaps of information about the system can lead to future problems.

We follow a previous line of work that is detecting invariants as the system executes, Daikon for instance \cite{daikon}. By instrumenting the program and analyzing the output we can infer invariants that are not easy to notice for developers and that expand with an increasing complexity.  

Section~\ref{Background} provides the necessary background to understand the approach that we took on this research. Section~\ref{Approach}  divides our approach in stages of the process of invariant inference. Section~\ref{Evaluation} presents the tool working on two cases; showcasing the architecture invariant inference on a TurtleBot and showcasing numeric value range and temporal sequence in a small random addition/multiplier tool.

\section{Background}
The following are brief descriptions of the tools and features that we used in order to accomplish our goal:
\label{Background}
\subsubsection{ROS (Robot Operating System)}
Is a framework that facilitates the development of robotic applications. From a high level perspective, it enables developers to separate complex actions into pieces of functionality, encapsulated in nodes. Nodes communicate using topics and services. Topics require a subcriber and a publisher for it to function as a communication method. Services in  the other hand can be hosted and function without any direct reciever. For services to serve as a comunication method they must be called by a client, this process is named service calls. 

\subsubsection{Invariants}
Are conditions that can be trusted to hold in the execution of a program or in program points. They are relevant  because they can help debug, validate and escalate a program. In our reserach we focused on architectural invariants. 

\subsubsection{Daikon}
Is a tool that helps us infer invariants by reporting only properties that hold throughout specific program points. Daikon can be extended to find new invariants by introducing templates. In our investigation we experimented with three templates, furthermore in section IV. In our research we treated nodes that publish or subscribe to a topic and service calls as program points.

Architectural invariants in  ROS systems can be identified as constant relations between topics and nodes. These relations are nodes that publish or subscribe to a topic during the execution of the program. The connections that were present during all program points are to be presented as candidate invariants. Nodes can also communicate using services, but there is a difference between both methods. Services do not require confirmation of a receiver, which means they do not require a targeted receiver. The solution to this obstacle is presented in section III .



\begin{figure}
\includegraphics[scale=0.25]{ProcessDiagram}
\caption{Diagram of the approach taken in this research. Note that instrumented ROS program represents the recording nodes working in a system.}
\label{fig:Approach}
\end{figure}

\section{Approach}
\label{Approach}
To identify and clarify our recording and inference technique, we divided the process into four steps (Figure~\ref{fig:Approach}); recording nodes, translation, Daikon extension and filtering. Note that filtering is still in an early stage and only removes invariants which can be safety considered irrelevant to the system. The following are the sections of the process:


\subsection{Recording Nodes}
In order to record the system's architecture, we start four nodes at the launch of the system: subscribers\_recorder, publishers\_recorder, service\_handler and rosbag. Each one of them have their own role in the recording process. We will start by describing their role in the process:
\subsubsection{rosbag}
Rosbag is a feature of ROS that could be compared to as a logger. Rosbag must be  initialized in order to allow our other nodes to report any changes or information to be later analyzed. Take note that rosbag was set to subscribe to all topics that are available. Therefore we must select only the publications of interest, these being the ones made by our recording nodes.
\subsubsection{subscriber\_recorder \& publishers\_recorder}
The  nodes take care of reporting the nodes that publish and/or subscribe to a topic. They do so by constantly requesting ROS master for this information. If the list of subscribers or publishers changes they publish to rosbag the updated list of connections. This information is critical when trying to infer architectural invariants on the system. \newline \newline




\begin{figure}
\includegraphics[scale=0.4] {Diagram}


\caption{Diagram of service\_hanlder interacting with the system. Node Caller calls service then service\_handler publishes the content then it calls Service\_prime. When a response is obtained it publishes the response and returns the content to the original caller.}
\label{fig:ServiceHandler}
\end{figure}


\subsubsection{service\_handler}
\normalsize
The service\_handler node is more complicated than the previous two, since ROS does not provide a direct way of recording service calls. We solved this problem by using an intermediary in the call process. Meaning that we trick the caller to believe that our node serves the service that they are referring to. The intermediary receives a call, it publishes the data to rosbag then it calls the original receiver and when it gets an answer it publish its and responds to the caller the same response obtained by the original receiver. In order for our node to accomplish such we had to generate a clone for each present service. We modified the source code of ROS to add a flag to the service name. This flag being “\_prime”. We made an exception for loggers, since they do not have an impact on the system's functionality. This flag applies only for services that are not register by service\_handler, and allows us to identify which services have to be cloned. Figure~\ref{fig:ServiceHandler} \&~\ref{fig:ListServices}, show the relationship that service\_handler has with other nodes/services (nodes calling services). This approach allows us to have full control of the data flow in service calls, we can obtain information such as time of a call, time intervals between calls and all the data being transmitted.

\begin{figure}[h!]

 \begin{tabular}{l c} 
/app\_manager/get\_loggers \\
/app\_manager/set\_logger\_level \\
/bumper2pointcloud/get\_loggers \\
/bumper2pointcloud/set\_logger\_level \\
/capability\_server/establish\_bond \\
\textbf {/capability\_server/establish\_bond\_prime} \\
/capability\_server/free\_capability \\
\textbf {/capability\_server/free\_capability\_prime} \\
/capability\_server/get\_capability\_spec \\
\textbf {/capability\_server/get\_capability\_spec\_prime} \\ [1ex] 
 \end{tabular}
\caption{List of services showing both original service and clone. Note that loggers do not have a prime}
\label{fig:ListServices}
\end{figure}

When a service is called, service\_handler generates a publisher with the same name as the service that was called. This publisher serves for two purposes; one it allows to have a direct channel of communication with rosbag that only reports information related to the service and second it allows our trace translator to treat each call as a program point. 

The service\_handler node also reports what node is serving a service and any changes throughout the program. This is important because it allows us to identify the amount of nodes that usually provide a service.  This publisher is also reported as a program point. 

As mentioned before rosbag is set to subscribe to all publishers meaning that we need to select only the publications that were made by our nodes. Since for every service call made to our services we start a publisher, we must have a list of the publishers generated by us to later be identified in rosbag. So service\_handler generates a list called “traceTranslationComplement” that contains our list of publishers.     
\subsection{Trace Translation}
The trace translation has the functionality of reading the rosbag output and retrieving all the publications made by our nodes. Primarily is set to find /rec/arch\_pub, /rec/arch\_sub and /rec/arch\_srvs. But since service\_handler generates more topics we take the list (traceTranslationComplement) of extra topics and add them to the list of important topics. 
	Trace Translator reports all the information of these topics as program points and formats them into a composition that Daikon can process. An example of a program point after translation would be: 

\begin{figure}[h!]
 \begin{tabular}{ l c} 
\textbf{/rec/arch\_srvs:::POINT}\\
data \\
"\{'capabilities/StartCapability': ['/capability\_server'],\\
 'capabilities/GetProviders': ['/capability\_server']\} "  \\ [1ex] 
 \end{tabular}
\end{figure}

\subsection{Daikon Extension}
\normalsize
We extended Daikon with three templates, each identifying a different type of possible invariant. These being: ArchitectureInvariant, ServiceCallInvariantFrequency and ServiceCallInvariantRangeNumericValues. Note that the last two templates are only for service call cases. While ArchitectureInvariant handles publishers, subscribers and services (node to service relation). We will be describing each of the templates in detail:  
\subsubsection{ArchitectureInvariant}
Identifies the minimum and maximum amount of nodes that publish/subscribe to a topic throughout the program's execution. In other words it analyzes all the program points of /rec/arch\_pub, /rec/arch\_sub and /rec/arch\_srvs setting the minimum to the least subscriptions/publications on a topic that were present on the program. And the maximum to the greatest amount of subscriptions/publications on a topic. In the case of node (server) to service relation, the minimum would be the least amount of nodes that provided certain service, while the maximum would be the greatest amount of nodes that provided a service.  An output example for /rec/arch\_srvs (services) would be:

\begin{figure}[h!]
 \begin{tabular}{l c} 
/rec/arch\_srvs:::POINT \\
\scriptsize
Max: \{'AddTwoInts': '[/add\_two\_ints\_server, /add\_two\_ints\_server2]',\} \\
\scriptsize
Min: \{'AddTwoInts': '[/add\_two\_ints\_server­]',\} \\ [1ex] 
 \end{tabular}
\caption{AddTwoInts being the service and next to it the servers that provided such service.}
\end{figure}

\normalsize
\subsubsection{ServiceCallInvariantFrequency}
Every service call reported contains a timestamp. This template analyzes the timestamps and reports information such as: number of calls made, sequence time, longest interval, shortest interval and the average interval. An output example:  \newline
\begin{figure}[h!]
 \begin{tabular}{l c} 
Milliseconds::: \\
Number of Calls:       500.0 \\
Sequence Time:  424262.0 \\
Longest Interval:      938.0 \\ 
Shortest Interval:     844.0 \\
Average Interval:     853.6458752515091 \\ [1ex] 
 \end{tabular}
\caption{Output of ServiceCallInvariantFrequency template. Note that the time is in milliseconds  }
\label{fig:ServiceCallFrequency}
\end{figure}
\normalsize

This report is not directly an invariant but it can be useful to detect anomalies in the system, or in the case of a known invariant this data may help identify the source of a problem. 
\subsubsection{ServiceCallInvariantRangeNumericValues}
Looks for numeric values in the service calls made. It constantly keeps comparing the values found in the program points to find the minimum and the maximum. Services can have multiple requirements (arguments) and responses. This template looks for both cases and reports for each one of the cases. An output example: 
\begin{table}[h!]

 \begin{tabular}{|c|r|r|} 
\hline
Type & Max & Min  \\
\hline
Response:: & - & - \\
Multiplication: & 8.74290615552E15 &  -5.54820673758E15 \\
Sum:  & 32194.1181099 &  -38688.8510291  \\ 

Requirements::  & - & -\\
a: & 9931.93460557  & -9958.61406644\\
b: & 9962.79277972 & -9994.90205016 \\
c: & 9998.23701324  & -9986.02595623 \\
d: & 9996.26538193 & -9980.82872157 \\ [1ex] 
\hline
 \end{tabular}
\caption{ Output of ServiceCallInvariantRangeNumericValues template}
\label{tab:ServiceCallRangeNumeric}
\end{table}

\normalsize
We can see that in Table~\ref{tab:ServiceCallRangeNumeric}  if you were to add all the maximums or minimums from requirements the result would not add up to the maximum or minimum sum response value.  This is because not all specific values were used in one call, they were possibly used in unrelated calls. So the maximum and minimum values are relatively independent from each other. 
\subsection{Filtering and Output Classification}
After obtaining a report from Daikon with the candidate invariants, we want to classify and if possible reduce the output by removing the invariants that are clearly irrelevant to the system's functionality. As mentioned before the filter is in early stages and only removes irrelevant data such as candidate invariants generated by our own implementation.  In other words, the current filter ignores the presence of our nodes, topics and/if present services. Rosbag data is also ignored otherwise we would see that rosbag always subscribes to all topics. 

The filter also allows the user to add any exception they want to make. If a developer is aware of an irrelevant state, meaning a node or topic, there is a list in which the name can be added in order for it to be ignored. 

Daikon's output can be somewhat extensive and presents the candidate invariants from a minimum and maximum perspective. This means that identifying a specific topic can be tedious work. Our filter analyzes the data and presents it from a topic perspective. Allowing the localization of specific topics easier.  
While testing the filter we found that the reports that were generated by ArchitectureInvariant could be classified. We decided to name these classifications or states with the purpose of easy reference and possible correlation with the system's dependency on their correct functionality. These classifications or states are: static, variable and restricted variable. Each one of them represent a specific characteristic of node to topic relation. By classifying them we can understand better their role in the system's architecture. Note that for services we are analyzing the number of servers that offered a specific service. The state definitions and corresponding examples are present in Figure~\ref{fig:ArchitecturalStates}.
\begin{figure}[h!]
 \begin{tabular}{lc } 

\textbf{Static:::} \\
\small
Minimum and maximum amount of nodes publishing/subscribing \\
\small
to a topic are equal to each other.  \\
Min = Max \\
\underline{Example::}  \\
turtlebot\_teleop/AddTwoInts:: \\
Min = Max |  ['/turtlebot\_teleop\_keyboard'] |  1 \\
\hline

\textbf{Variable:::} \\
\small
The minimum amount of nodes publishing/subscribing to a topic \\
\small
 is zero and the maximum is greater than zero. \\
Min = 0 \\
Max \textgreater \ 0 \\
\underline{Example::}  \\
/rosout\_agg:: \\
Min: [] | 0 \\
Max: ['/rosout', '/record\_1469033124769797118'] | 2 \\

\hline
\textbf{Restricted Variable:::} \\
\small
The minimum amount of nodes publishing/subscribing to a topic \\
\small
is greater than zero and the maximum is the minimum plus n \\
number of extra nodes. \\
Min \textgreater \  0 \\
Max = Min + n \\
\underline{Example::}  \\
/rosout:: \\
\small
Min: ['/record\_1469033124769797118'] | 1 \\
\small
Max: Min + ['/master', '/capability\_server\_nodelet\_manager',  \\
\small
'/robot\_state\_publisher', '/diagnostic\_aggregator', \\
\small
 '/zeroconf/zeroconf', '/cmd\_vel\_mux', \\
\small
 '/mobile\_base\_nodelet\_manager', '/interactions', \\
\small
'/app\_manager', '/bumper2pointcloud'] | 11 \\ 
\hline

 \end{tabular}
\caption{Definitions of the three states that are present in node to topic relation. In the case of services we analize the server to service relation.}
\label{fig:ArchitecturalStates}
\end{figure}

\section{Evaluation}
\label{Evaluation}
To evaluate our approach we instrumented two systems with our recording nodes. One being the a functioning TurtleBot and the second being a test case that used service calls with numeric values as requirements and response. In the case of the TurtleBot we were more focused on the possible result of the ArchitectureInvariant template, this because TurtleBot does not use service calls as its main form of communication. For the second case we strictly wanted to test the service call templates. In both cases we were able to record service calls, node to topic and server to service relation. 

\subsection{TurtleBot}
The proceedings in testing our approach on TurtleBot were the following. We started a record launch file, which contains the initialization of our nodes in sequence, beginning with Rosbag.  When we had the recording nodes up and running we started the TurtleBot with \textit{roslaunch turtlebot\_bringup minimal}. Service\_handler started creating the service clones as can be appreciated in Fig~\ref{fig:ListServices}, note that this is a small portion of the total list of services. In the case of subscribers\_recorder and publishers\_recorder we had to proof their correct functionality by revising their output after analyzing rosbag. We confirmed the correct connections by running commans like \textit{rosservice list} and \textit{rostopic info} and comparing the outputs of rosbag. All recording nodes were functioning properly. In order to have more interaction with the turtlebot and to get more data we ran \textit{roslaunch turtlebot\_teleop keyboard\_teleop} which basically allows the user to control the TurtleBot with the keyboard. 

\subsection{Service Call Test Case}
We developed a small test case based on a tutorial available in the ROS web page. The tutorial consists of one node that serves a service called AddTwoInts and a client that calls it. AddTwoInts takes as arguments two integers and returns an integer which is the addition of the both received. We extended this example to take and return doubles. The extended version made 500 calls with one second interval. Also all the values used were randomly generated from -10000 to 10000. The extension also include extra return and argument values.  We increased the arguments by two and added as return a multiplication. Figure~\ref{tab:ServiceCallRangeNumeric} \&~\ref{fig:ServiceCallFrequency} show outputs from the reports. 
\newline

We were able to appreciate the targeted connections in both of our test cases. The relevancy of our findings stands firmly in the application of such. Considering the first case with the TurtleBot, the report can work as a guide of connections to be careful modifying. For the second case we were able to find values that represented the correct functionality of a program. The second case was small but it is easy to see its usefulness. From detecting abnormal behavior in a system to detecting its possible unchangeable conditions.
 

\section{Conclusion}
Many of the conditions that we found are not to be considered invariants right away. Our tool detects candidate invariants but grater development must be done to infer the most relevant ones. Currently there is no direct way of knowing which invariants are key to the system. In our search of understanding our report we broke each candidate invariant and found that characteristics such as the name of the node or topic can have insight about its relevancy. In the case of Turtlebot, topics with the name manager when broken tended to crash the system more often, meaning that it carried more weight (relevancy) in the system. It is obvious that it is not the most appropriate way to look at it but it provides a sense of understanding in such early stages.

To fulfill the potential of invariant inference in ROS systems we must expand in three main areas; templates, filtering and testing. Expanding on these areas will allow us to infer more relevant and useful invariants. 


\subsection{Future Work}
\subsubsection{Template Expansion}
There are still many possible invariants to be found, currently we are only inferring with three templates. A next step would be to introduce new templates that deal with the actual data being transferred between nodes that use topics as their communication method. 

\subsubsection{Filtering and Testing}
A big part of understanding the report generated by our tool is to test which candidate invariant is an actual invariant. Currently we test our report by  breaking case by case and monitoring the reaction of the system. This can be tedious work, and it could be done automatically. By having automatic testing we can improve filtering.  \newline 

\indent Architectual invariant inference is a great first step in a long process for a self adaptive system in which invariant discovery is key for its development and functionality.

\section*{Acknowledgment}

This research was supported by the National Science Foundation. I thank our colleagues from Carnegie Mellon University who provided insight and expertise that greatly assisted the research. I thank Afsoon Afzal for her extensive contribution in this research, and Ph.D Claire Le Goues for her guidance and insight in every step of this research.


\bibliography{references}
\bibliographystyle{plain}


% that's all folks
\end{document}


